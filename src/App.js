/* eslint-disable jsx-a11y/anchor-is-valid */

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import FlightDetails from "./components/FlightDetails/FlightDetails";

function App() {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/details">
            <FlightDetails />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
