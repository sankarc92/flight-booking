import { combineReducers } from "redux";
import flightsReducer from "./components/Search/reducer";

export default combineReducers({
  searchFlights: flightsReducer,
});
