/* eslint-disable jsx-a11y/anchor-is-valid */
const Header = () => {
  return (
    <nav className="navbar navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand" href="#">
          <h1> Flight Booking System </h1>
        </a>
      </div>
    </nav>
  );
};

export default Header;
