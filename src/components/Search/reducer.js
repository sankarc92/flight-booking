/* eslint-disable import/no-anonymous-default-export */
import * as FLIGHTS_TYPE from "./type";

const intitalState = {
  page: 0,
  allRecords: [],
  currentRecords: [],
  totalPages: 0,
};

export default function (state = intitalState, action) {
  switch (action.type) {
    case FLIGHTS_TYPE.FETCH_FLIGHTS:
      return {
        ...state,
        allRecords: action.payload,
        totalPages: Math.floor(action.payload.length / 10),
      };
    case FLIGHTS_TYPE.SET_CURRENT_RECORDS:
      return {
        ...state,
        currentRecords: action.payload,
      };
    default:
      return state;
  }
}
