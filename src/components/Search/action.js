import * as FLIGHTS_TYPE from "./type";

export const searchFlights = (params) => (dispatch, getState) => {
  // TODO DATE FILTER
  const state = getState();
  const allRecords = state.searchFlights.allRecords;
  const filterItems = allRecords.filter((item) => {
    if (
      item.source === params.source &&
      item.destination === params.destination
    ) {
      return true;
    } else {
      return false;
    }
  });
  dispatch({
    type: "SET_CURRENT_RECORDS",
    payload: filterItems,
  });
};

export const fetchFlights = () => (dispatch) => {
  fetch("http://localhost:3000/sample.json")
    .then((res) => res.json())
    .then((res) =>
      dispatch({
        type: FLIGHTS_TYPE.FETCH_FLIGHTS,
        payload: res,
      })
    );
};
