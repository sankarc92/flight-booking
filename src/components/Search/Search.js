import { useState } from "react";
import { useDispatch } from "react-redux";
import { searchFlights } from "./action";

const SearchFlights = () => {
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    source: "",
    destination: "",
    travelDate: "",
    returnDate: "",
  });

  const handleInput = (e) => {
    console.log(filters);
    setFilters({
      ...filters,
      [e.target.name]: e.target.value,
    });
  };

  const reset = () => {
    setFilters({
      source: "",
      destination: "",
      travelDate: "",
      returnDate: "",
    });
  };

  const handleSubmit = () => {
    dispatch(searchFlights(filters));
  };

  return (
    <div className="row">
      <div className="col-md-12">
        <div class="search-container">
          <h3> Search Flights</h3>
          <div className="row">
            <div class="col-md-3">
              <label for="sourceCity">
                Source City <span className="red">*</span>
              </label>
              <input
                type="text"
                class="form-control"
                id="sourceCity"
                placeholder="Source City"
                value={filters.source}
                name="source"
                onChange={handleInput}
              />
            </div>
            <div class="col-md-3">
              <label for="sourceCity">
                Destination City<span className="red">*</span>
              </label>
              <input
                type="text"
                class="form-control"
                id="destinationCity"
                placeholder="Destination City"
                value={filters.destination}
                name="destination"
                onChange={handleInput}
              />
            </div>
            <div class="col-md-3">
              <label for="sourceCity">
                Travel Date<span className="red">*</span>
              </label>
              <input
                type="date"
                class="form-control"
                id="travelDate"
                placeholder="Travel Date"
                value={filters.travelDate}
                name="travelDate"
                onChange={handleInput}
              />
            </div>
            <div class="col-md-3">
              <label for="sourceCity">Return Date</label>
              <input
                type="date"
                class="form-control"
                id="retunDate"
                placeholder="Return Date"
                value={filters.returnDate}
                name="returnDate"
                onChange={handleInput}
              />
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="action-group">
            <button
              type="button"
              class="btn btn-dark"
              onClick={handleSubmit}
              disabled={
                !filters.source || !filters.destination || !filters.travelDate
              }
            >
              Search
            </button>
            <button type="button" class="btn btn-light" onClick={reset}>
              Reset
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchFlights;
