/* eslint-disable jsx-a11y/anchor-is-valid */
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import SearchFlights from "../Search/Search";
import { fetchFlights } from "../Search/action";

const formatDate = (date) => {
  date = new Date(date);
  return (
    date.getDate() +
    "/" +
    (date.getMonth() + 1) +
    "/" +
    date.getFullYear() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  );
};

function Home() {
  const dispatch = useDispatch();
  const allRecords = useSelector((state) => state.searchFlights.allRecords);
  const totalPages = useSelector((state) => state.searchFlights.totalPages);
  const currentRecords = useSelector(
    (state) => state.searchFlights.currentRecords
  );

  const [page, setPage] = useState(0);

  useEffect(() => {
    dispatch(fetchFlights());
  }, [dispatch]);

  useEffect(() => {
    const currentIndex = page * 10;
    const endIndex = (page + 1) * 10;
    console.log(currentIndex, endIndex);
    const data = allRecords.slice(currentIndex, endIndex);
    dispatch({
      type: "SET_CURRENT_RECORDS",
      payload: data,
    });
  }, [page, allRecords, dispatch]);

  return (
    <div>
      <div className="container">
        <div className="row">
          <SearchFlights />
          <div className="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Flight Number</th>
                  <th scope="col">Airline Name</th>
                  <th scope="col">Departure Time</th>
                  <th scope="col">Arrival Time</th>
                  <th scope="col">Duration</th>
                  <th scope="col">No. Of Stops</th>
                  <th scope="col">Price</th>
                </tr>
              </thead>
              <tbody>
                {currentRecords.map((row, index) => (
                  <tr>
                    <th scope="row">
                      {" "}
                      <Link to="/details"> {row.flightName} </Link>
                    </th>
                    <td>{row.airlineName}</td>
                    <td>{formatDate(row.depatureTime)}</td>
                    <td>{formatDate(row.arrivalTime)}</td>
                    <td>{row.duration}</td>
                    <td>{row.noOfStops}</td>
                    <td>{row.price}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="col-md-12">
            <div className="pagination">
              <button
                type="button"
                class="btn btn-dark"
                onClick={() => setPage(page - 1)}
                disabled={page === 0 ? true : false}
              >
                Prev
              </button>
              <button
                type="button"
                class="btn btn-dark"
                onClick={() => setPage(page + 1)}
                disabled={page === totalPages ? true : false}
              >
                Next
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
